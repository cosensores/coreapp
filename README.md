## Herramienta para el relevamiento comunitario de datos relacionados a la salud y el ambiente en el territorio.  
  

﻿![foto](/Imagenes/app.jpeg)
  
﻿![foto](/Imagenes/planilla.png)
  
﻿![foto](/Imagenes/logo.png)

﻿Aplicación: /Aplicación android/CoReApp.apk  
﻿Aplicación editable[(kodular)](https://www.kodular.io/):/Aplicación android/CoReApp.aia  
﻿Fuentes: [Crazy Coder Club](https://www.crazycodersclub.com/) https://www.youtube.com/watch?v=7j8_3uIAQLk&t=609s
